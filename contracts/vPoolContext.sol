// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./Types256.sol";
import "./vPoolMath.sol";

contract vPoolContext is vPoolMath {
    Pool[] rPools;
    address[] _tokens;

    address owner;

    constructor() {
        owner = msg.sender;
    }

    function getTokens() public view returns (address[] memory) {
        return _tokens;
    }

    function getPoolsCount() public view returns (uint256) {
        return rPools.length;
    }

    function getPoolIndex(address tokenA, address tokenB)
        public
        view
        returns (uint256)
    {
        uint256 index = 999;

        for (uint256 i = 0; i < rPools.length; i++) {
            if (rPools[i].tokenA == tokenA && rPools[i].tokenB == tokenB) {
                index = i;
                break;
            }
        }

        return index;
    }

    function tokenExist(address token) public view returns (bool) {
        for (uint256 i = 0; i < _tokens.length; i++) {
            if (_tokens[i] == token) {
                return true;
            }
        }

        return false;
    }
}

