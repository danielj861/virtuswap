// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./vPoolContext.sol";
import "./ERC20/IERC20.sol";
import "./ERC20/ERC20.sol";

contract vPoolManager is vPoolContext {
    function _calculateBelowThreshold()
        internal
        view
        returns (int[] memory)
    {
        int[] memory reserveRatio = _calculateReserveRatio();

        int[] memory belowThreshold = new int[](rPools.length);

        for (uint256 i = 0; i < rPools.length; i++) {
            if (
                reserveRatio[i] >= rPools[i].maxReserveRatio &&
                belowThreshold[i] == 1
            ) {
                belowThreshold[i] = 0;
            } else if (reserveRatio[i] == 0) {
                belowThreshold[i] = 1;
            }
        }

        return belowThreshold;
    }

    function _calculateReserveRatio() internal view returns (int[] memory) {
        int[] memory reserveRatio = new int[](rPools.length);

        for (uint i = 0; i < rPools.length; i++) {
            for (uint k = 0; k < _tokens.length; k++) {
                if (
                    rPools[i].tokenA == _tokens[k] ||
                    rPools[i].tokenB == _tokens[k]
                ) continue;

                uint ikIndex = getPoolIndex(rPools[i].tokenA, _tokens[k]);

                uint jkIndex = getPoolIndex(rPools[i].tokenB, _tokens[k]);

                if (ikIndex == 999 || jkIndex == 999) continue;

                reserveRatio[i] = 0;
                reserveRatio[i] =
                    reserveRatio[i] +
                    (rPools[i].reserves[_tokens[k]].reserveBalance *
                        max(
                            rPools[ikIndex].tokenABalance /
                                max(rPools[ikIndex].tokenBBalance, epsilon),
                            ((rPools[jkIndex].tokenABalance /
                                max(rPools[jkIndex].tokenBBalance, epsilon)) *
                                rPools[i].tokenABalance) /
                                max(rPools[i].tokenBBalance, epsilon)
                        )) /
                    (2 * max(rPools[i].tokenABalance, epsilon));
            }
        }

        return reserveRatio;
    }

    function createPool(address tokenA, address tokenB) internal {
        rPools.push();
        Pool storage newPool = rPools[rPools.length - 1];
        newPool.tokenA = tokenA;
        newPool.tokenB = tokenB;
        newPool.fee = 0.002 ether;
        newPool.maxReserveRatio = 0.02 ether;

        if (!tokenExist(tokenA)) {
            _tokens.push(tokenA);
        }

        if (!tokenExist(tokenB)) {
            _tokens.push(tokenB);
        }

        //create LP token
        string memory name = string(abi.encodePacked("tt-LP", rPools.length));

        ERC20 LPToken = new ERC20(name, "VRSWP-LP");
        newPool.LPToken = address(LPToken);

        //mint first tokens to blackhole
        //    LPToken._mint(address(0), MINIMUM_LIQUIDITY);
    }

    function emptyLiquidity(address tokenAAddress, address tokenBAddress)
        public
    {
        uint256 poolIndex = getPoolIndex(tokenAAddress, tokenBAddress);

        if (msg.sender == owner && poolIndex < 999) {
            require(
                IERC20(tokenAAddress).transfer(
                    owner,
                    uint256(rPools[poolIndex].tokenABalance)
                ),
                "Failed to send token A"
            );
            rPools[poolIndex].tokenABalance = 0;

            require(
                IERC20(tokenBAddress).transfer(
                    owner,
                    uint256(rPools[poolIndex].tokenBBalance)
                ),
                "Failed to send token B"
            );
            rPools[poolIndex].tokenBBalance = 0;
        }
    }

    function deposiLiquidity(
        address tokenA,
        address tokenB,
        uint256 tokenAAmount,
        uint256 tokenBAmount
    ) public {
        IERC20 tokenAInstance = IERC20(tokenA);
        IERC20 tokenBInstance = IERC20(tokenB);

        uint256 poolIndex = getPoolIndex(tokenA, tokenB);

        if (poolIndex == 999) {
            createPool(tokenA, tokenB);
        }

        poolIndex = getPoolIndex(tokenA, tokenB);
        require(poolIndex != 999, "Could not find pool");

        require(
            tokenAInstance.transferFrom(
                msg.sender,
                address(this),
                tokenAAmount
            ),
            "Could not transfer token A"
        );
        require(
            tokenBInstance.transferFrom(
                msg.sender,
                address(this),
                tokenBAmount
            ),
            "Could not transfer token B"
        );

        rPools[poolIndex].tokenABalance =
            rPools[poolIndex].tokenABalance +
            int256(tokenAAmount);

        rPools[poolIndex].tokenBBalance =
            rPools[poolIndex].tokenBBalance +
            int256(tokenBAmount);

        //issue LP tokens
        ERC20(rPools[poolIndex].LPToken)._mint(msg.sender, tokenAAmount);
    }
}
