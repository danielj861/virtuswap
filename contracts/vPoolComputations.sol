// pragma solidity >=0.4.22 <0.9.0;

// import "./Types256.sol";

// contract VPoolComputation {
//     int256 epsilon = 1 wei;

//     function _calculateBelowThreshold(Pool[] memory rPools)
//         internal
//         view
//         returns (int256[] memory)
//     {
//         int256[] memory reserveRatio = _calculateReserveRatio();

//         int256[] memory belowThreshold = new int256[](rPools.length);

//         for (uint256 i = 0; i < rPools.length; i++) {
//             if (
//                 reserveRatio[i] >= rPools[i].maxReserveRatio &&
//                 belowThreshold[i] == 1
//             ) {
//                 belowThreshold[i] = 0;
//             } else if (reserveRatio[i] == 0) {
//                 belowThreshold[i] = 1;
//             }
//         }

//         return belowThreshold;
//     }

//     function getPoolIndex(
//         Pool[] memory rPools,
//         address tokenA,
//         address tokenB
//     ) internal view returns (uint256) {
//         uint256 index = 0;
//         bool found = false;
//         for (uint256 i = 0; i < rPools.length; i++) {
//             if (
//                 rPools[i].tokenA.tokenAddress == tokenA &&
//                 rPools[i].tokenB.tokenAddress == tokenB
//             ) {
//                 index = i;
//                 found = true;
//                 break;
//             }
//         }

//         return index;
//     }

//     function _calculateReserveRatio(Pool[] memory rPools, Token[] memory tokens)
//         internal
//         view
//         returns (int256[] memory)
//     {
//         int256[] memory reserveRatio = new int256[](rPools.length);

//         for (uint256 i = 0; i < rPools.length; i++) {
//             for (uint256 k = 0; k < tokens.length; k++) {
//                 if (
//                     rPools[i].tokenA.tokenAddress == tokens[k].tokenAddress ||
//                     rPools[i].tokenB.tokenAddress == tokens[k].tokenAddress
//                 ) continue;

//                 uint256 ikIndex = getPoolIndex(
//                     rPools[i].tokenA.tokenAddress,
//                     tokens[k].tokenAddress
//                 );

//                 uint256 jkIndex = getPoolIndex(
//                     rPools[i].tokenB.tokenAddress,
//                     tokens[k].tokenAddress
//                 );

//                 reserveRatio[i] = 0;
//                 reserveRatio[i] =
//                     reserveRatio[i] +
//                     (rPools[i].reserves[tokens[k].tokenAddress].reserveBalance *
//                         max(
//                             rPools[ikIndex].tokenABalance /
//                                 max(rPools[ikIndex].tokenBBalance, epsilon),
//                             ((rPools[jkIndex].tokenABalance /
//                                 max(rPools[jkIndex].tokenBBalance, epsilon)) *
//                                 rPools[i].tokenABalance) /
//                                 max(rPools[i].tokenBBalance, epsilon)
//                         )) /
//                     (2 * max(rPools[i].tokenABalance, epsilon));
//             }
//         }

//         return reserveRatio;
//     }

//     function max(int256 a, int256 b) internal pure returns (int256) {
//         return a >= b ? a : b;
//     }

//     function min(int256 a, int256 b) internal pure returns (int256) {
//         return a < b ? a : b;
//     }
// }
