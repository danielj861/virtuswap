pragma solidity >=0.4.22 <0.9.0;

struct Token {
    address tokenAddress;
    int256 price;
    string name;
}

struct VirtualPool {
    uint256 id;
    string tokenAName;
    string tokenBName;
    int256 fee;
    int256 tokenABalance;
    int256 tokenBBalance;
    bool balanced;
}

struct VirtualPoolVM {
    string tokenAName;
    string tokenBName;
    int256 fee;
    int256 tokenABalance;
    int256 tokenBBalance;
}

struct Pool {
    address tokenA;
    address tokenB;
    address LPToken;
    int256 fee;
    int256 tokenABalance;
    int256 tokenBBalance;
    int256 maxReserveRatio;
    mapping(address => ReserveBalance) reserves;
}

struct PoolVM {
    address tokenA;
    address tokenB;
        address LPToken;
    int256 fee;
    int256 reserveRatio;
    int256 belowReserve;
    int256 tokenABalance;
    int256 tokenBBalance;
    int256 maxReserveRatio;
    bool valid;
}

struct ReserveBalance {
    address tokenAddress;
    string tokenName;
    int256 reserveBalance;
}
