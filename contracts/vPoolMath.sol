// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./Types256.sol";

contract vPoolMath {
    int256 public epsilon = 1 wei;

    uint256 public constant MINIMUM_LIQUIDITY = 10**3;

    int256 imbalance_tolerance_base = 0.01 ether;

    function max(int256 a, int256 b) public pure returns (int256) {
        return a >= b ? a : b;
    }

    function min(int256 a, int256 b) public pure returns (int256) {
        return a < b ? a : b;
    }
}


