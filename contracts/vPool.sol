// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./Types256.sol";
import "./ERC20/IERC20.sol";
import "./ERC20/ERC20.sol";

library vPoolCalculations {
    function max(int256 a, int256 b) internal pure returns (int256) {
        return a >= b ? a : b;
    }

    function min(int256 a, int256 b) internal pure returns (int256) {
        return a < b ? a : b;
    }

    function getPoolIndex(
        Pool[] storage rPools,
        address tokenA,
        address tokenB
    ) internal view returns (uint256) {
        uint256 index = 999;

        for (uint256 i = 0; i < rPools.length; i++) {
            if (rPools[i].tokenA == tokenA && rPools[i].tokenB == tokenB) {
                index = i;
                break;
            }
        }

        return index;
    }

    function tokenExist(address[] memory tokens, address token)
        internal
        pure
        returns (bool)
    {
        for (uint256 i = 0; i < tokens.length; i++) {
            if (tokens[i] == token) {
                return true;
            }
        }

        return false;
    }

    function _calculateReserveRatio(
        Pool[] storage rPools,
        address[] memory tokens
    ) public view returns (int256[] memory) {
        int256 epsilon = 1 wei;

        int256[] memory reserveRatio = new int256[](rPools.length);

        for (uint256 i = 0; i < rPools.length; i++) {
            for (uint256 k = 0; k < tokens.length; k++) {
                if (
                    rPools[i].tokenA == tokens[k] ||
                    rPools[i].tokenB == tokens[k]
                ) continue;

                uint256 ikIndex = getPoolIndex(
                    rPools,
                    rPools[i].tokenA,
                    tokens[k]
                );

                uint256 jkIndex = getPoolIndex(
                    rPools,
                    rPools[i].tokenB,
                    tokens[k]
                );

                if (ikIndex == 999 || jkIndex == 999) {
                    reserveRatio[i] = 0;
                    continue;
                }

                reserveRatio[i] = 0;
                reserveRatio[i] =
                    reserveRatio[i] +
                    (rPools[i].reserves[tokens[k]].reserveBalance *
                        max(
                            rPools[ikIndex].tokenABalance /
                                max(rPools[ikIndex].tokenBBalance, epsilon),
                            ((rPools[jkIndex].tokenABalance /
                                max(rPools[jkIndex].tokenBBalance, epsilon)) *
                                rPools[i].tokenABalance) /
                                max(rPools[i].tokenBBalance, epsilon)
                        )) /
                    (2 * max(rPools[i].tokenABalance, epsilon));
            }
        }

        return reserveRatio;
    }

    function _calculateVirtualPools(
        Pool[] storage rPools,
        address[] memory tokens
    ) public view returns (VirtualPool[] memory) {
        int256 epsilon = 1 wei;
        int256 imbalance_tolerance_base = 0.01 ether;

        VirtualPool[] memory vPools = new VirtualPool[](rPools.length);
        int256[] memory belowReserve = _calculateBelowThreshold(rPools, tokens);

        for (uint256 i = 0; i < rPools.length; i++) {
            for (uint256 k = 0; k < tokens.length; k++) {
                if (
                    rPools[i].tokenA == tokens[k] ||
                    rPools[i].tokenB == tokens[k]
                ) continue;

                uint256 ikIndex = getPoolIndex(
                    rPools,
                    rPools[i].tokenA,
                    tokens[k]
                );

                uint256 jkIndex = getPoolIndex(
                    rPools,
                    rPools[i].tokenB,
                    tokens[k]
                );

                int256 ikIndexTokenABalance = 0;
                int256 ikIndexTokenBBalance = 0;
                int256 jkIndexTokenABalance = 0;
                int256 jkIndexTokenBBalance = 0;
                vPools[i].fee = 0.003 ether;

                if (ikIndex == 999 || jkIndex == 999) {
                    continue;
                }

                if (ikIndex < 999) {
                    ikIndexTokenABalance = rPools[ikIndex].tokenABalance;
                    ikIndexTokenBBalance = rPools[ikIndex].tokenBBalance;
                }
                if (jkIndex < 999) {
                    jkIndexTokenABalance = rPools[jkIndex].tokenABalance;
                    jkIndexTokenBBalance = rPools[jkIndex].tokenBBalance;
                }

                // if () {

                //     vPools[i].tokenBBalance = 0;
                // }

                // vPools[i].tokenAName = rPools[i].tokenA.name;
                // vPools[i].tokenBName = rPools[i].tokenB.name;

                //  V(i,j,i)=V(i,j,i)+ind_below_reserve_threshold(i,k)*R(i,k,i)*min(R(i,k,k),R(j,k,k))/max(R(i,k,k),epsilon);
                // vPools[i].tokenABalance =
                //     vPools[i].tokenABalance +
                //     (belowReserve[i] *
                //         ikIndexTokenABalance *
                //         min(ikIndexTokenBBalance, jkIndexTokenBBalance)) /
                //     max(ikIndexTokenBBalance, epsilon);

                // //  V(i,j,j)=V(i,j,j)+ind_below_reserve_threshold(i,k)*R(j,k,j)*min(R(i,k,k),R(j,k,k))/max(R(j,k,k),epsilon);
                // vPools[i].tokenBBalance =
                //     vPools[i].tokenBBalance +
                //     (belowReserve[i] *
                //         jkIndexTokenABalance *
                //         min(ikIndexTokenBBalance, jkIndexTokenBBalance)) /
                //     max(jkIndexTokenBBalance, epsilon);
            }

            // int256 imbalanceRatio = (vPools[i].tokenABalance /
            //     vPools[i].tokenBBalance) /
            //     (rPools[i].tokenABalance / rPools[i].tokenBBalance);

            // vPools[i].balanced =
            //     (imbalanceRatio > 1 + imbalance_tolerance_base) ||
            //     (imbalanceRatio < 1 - imbalance_tolerance_base);
        }

        return vPools;
    }

    function getTPools(Pool[] storage rPools, address[] memory _tokens)
        public
        view
        returns (VirtualPoolVM[] memory)
    {
        VirtualPool[] memory vPools = _calculateVirtualPools(rPools, _tokens);
        VirtualPoolVM[] memory temp = new VirtualPoolVM[](rPools.length);
        for (uint256 i = 0; i < rPools.length; i++) {
            temp[i].fee = vPools[i].fee;

            temp[i].tokenABalance =
                rPools[i].tokenABalance +
                vPools[i].tokenABalance;

            temp[i].tokenBBalance =
                rPools[i].tokenBBalance +
                vPools[i].tokenBBalance;

            temp[i].tokenAName = vPools[i].tokenAName;
            temp[i].tokenBName = vPools[i].tokenBName;

            if (temp[i].tokenABalance > 0) {
                temp[i].fee =
                    (rPools[i].fee *
                        rPools[i].tokenABalance +
                        vPools[i].fee *
                        vPools[i].tokenABalance) /
                    temp[i].tokenABalance;
            }
        }
        return temp;
    }

    function getTotalsPool(Pool[] storage rPools, address[] memory _tokens)
        public
        view
        returns (VirtualPoolVM[] memory)
    {
        VirtualPool[] memory vPools = _calculateVirtualPools(rPools, _tokens);
        VirtualPoolVM[] memory temp = new VirtualPoolVM[](rPools.length);
        for (uint256 i = 0; i < rPools.length; i++) {
            temp[i].fee = vPools[i].fee;

            temp[i].tokenABalance =
                rPools[i].tokenABalance +
                vPools[i].tokenABalance;

            temp[i].tokenBBalance =
                rPools[i].tokenBBalance +
                vPools[i].tokenBBalance;

            temp[i].tokenAName = vPools[i].tokenAName;
            temp[i].tokenBName = vPools[i].tokenBName;
        }
        return temp;
    }

    function _calculateBelowThreshold(
        Pool[] storage rPools,
        address[] memory tokens
    ) internal view returns (int256[] memory) {
        int256[] memory reserveRatio = _calculateReserveRatio(rPools, tokens);

        int256[] memory belowThreshold = new int256[](rPools.length);

        for (uint256 i = 0; i < rPools.length; i++) {
            if (
                reserveRatio[i] >= rPools[i].maxReserveRatio &&
                belowThreshold[i] == 1
            ) {
                belowThreshold[i] = 0;
            } else if (reserveRatio[i] == 0) {
                belowThreshold[i] = 1;
            }
        }

        return belowThreshold;
    }

    function costVirtuswap(
        Pool[] storage rPools,
        address[] memory tokens,
        address buy_currency,
        address sell_currency,
        int256 amount
    ) public view returns (int256) {
        VirtualPoolVM[] memory tPools = getTPools(rPools, tokens);
        uint256 tradePoolIndex = vPoolCalculations.getPoolIndex(
            rPools,
            buy_currency,
            sell_currency
        );

        int256 lagTTokenABalance = tPools[tradePoolIndex].tokenABalance;
        int256 lagTTokenBBalance = tPools[tradePoolIndex].tokenBBalance;

        /*
        T_virtuswap(buy_currency,sell_currency,buy_currency,time)=lag_T(buy_currency,sell_currency,buy_currency)-Buy*(1-lag_fee_T(buy_currency,sell_currency));
        */
        tPools[tradePoolIndex].tokenABalance =
            lagTTokenABalance -
            (amount - ((tPools[tradePoolIndex].fee * amount) / 1 ether));

        // T(buy_currency,sell_currency,sell_currency)=lag_T(buy_currency,sell_currency,buy_currency)*lag_T(buy_currency,sell_currency,sell_currency)/(lag_T(buy_currency,sell_currency,buy_currency)-Buy); // %calculate amount_out
        tPools[tradePoolIndex].tokenBBalance =
            (lagTTokenABalance * lagTTokenBBalance) /
            (lagTTokenABalance - amount);

        int256 calcA = tPools[tradePoolIndex].tokenBBalance - lagTTokenBBalance;
        int256 calcB = (amount * lagTTokenBBalance) / lagTTokenABalance;
        int256 calcD = (calcA - calcB);

        calcD = calcD * 10000;
        int256 cost = calcD / calcB;

        return cost;
    }

    function costUniswapDirect(
        Pool[] storage rPools,
        address buy_currency,
        address sell_currency,
        int256 amount
    ) public view returns (int256) {
        uint256 tradePoolIndex = getPoolIndex(
            rPools,
            buy_currency,
            sell_currency
        );

        int256 lagRTokenABalance = rPools[tradePoolIndex].tokenABalance;
        int256 lagRTokenBBalance = rPools[tradePoolIndex].tokenBBalance;

        // T(buy_currency,sell_currency,sell_currency)=lag_T(buy_currency,sell_currency,buy_currency)*lag_T(buy_currency,sell_currency,sell_currency)/(lag_T(buy_currency,sell_currency,buy_currency)-Buy); // %calculate amount_out
        int256 tradePoolTokenBBalance = (lagRTokenABalance *
            lagRTokenBBalance) / (lagRTokenABalance - amount);

        int256 calcA = tradePoolTokenBBalance - lagRTokenBBalance;
        int256 calcB = (amount * lagRTokenBBalance) / lagRTokenABalance;
        int256 calcD = (calcA - calcB);

        calcD = calcD * 10000;
        int256 cost = calcD / calcB;
        return cost;
    }

    function quote(
        Pool[] storage rPools,
        address[] memory tokens,
        address buy_currency,
        address sell_currency,
        int256 amount
    ) public view returns (int256) {
        VirtualPoolVM[] memory tPools = getTPools(rPools, tokens);
        uint256 tradePoolIndex = vPoolCalculations.getPoolIndex(
            rPools,
            buy_currency,
            sell_currency
        );

        int256 lagTTokenABalance = tPools[tradePoolIndex].tokenABalance;
        int256 lagTTokenBBalance = tPools[tradePoolIndex].tokenBBalance;

        /*
        T_virtuswap(buy_currency,sell_currency,buy_currency,time)=lag_T(buy_currency,sell_currency,buy_currency)-Buy*(1-lag_fee_T(buy_currency,sell_currency));
        */
        tPools[tradePoolIndex].tokenABalance =
            lagTTokenABalance -
            (amount - ((tPools[tradePoolIndex].fee * amount) / 1 ether));

        // T(buy_currency,sell_currency,sell_currency)=lag_T(buy_currency,sell_currency,buy_currency)*lag_T(buy_currency,sell_currency,sell_currency)/(lag_T(buy_currency,sell_currency,buy_currency)-Buy); // %calculate amount_out
        tPools[tradePoolIndex].tokenBBalance =
            (lagTTokenABalance * lagTTokenBBalance) /
            (lagTTokenABalance - amount);

        int256 finalQuote = tPools[tradePoolIndex].tokenBBalance -
            lagTTokenBBalance;

        // emit Debug(
        //     "tPools[tradePoolIndex].tokenBBalance",
        //     tPools[tradePoolIndex].tokenBBalance
        // );
        // emit Debug("lagTTokenBBalance", lagTTokenBBalance);
        // emit Debug("finalQuote", finalQuote);

        return finalQuote;
    }

    // function swap(
    //     Pool[] storage rPools,
    //     address[] memory tokens,
    //     address buy_currency,
    //     address sell_currency,
    //     int256 amount
    // ) public {
    //     VirtualPoolVM[] memory tPools = getTPools(rPools, tokens);
    //     VirtualPoolVM[] memory lagR = new VirtualPoolVM[](rPools.length);

    //     uint256 tradePoolIndex = getPoolIndex(
    //         rPools,
    //         buy_currency,
    //         sell_currency
    //     );

    //     uint256 reverseTradePoolIndex = getPoolIndex(
    //         rPools,
    //         sell_currency,
    //         buy_currency
    //     );

    //     // %save current values

    //     //lag_T(buy_currency,sell_currency,buy_currency)=T(buy_currency,sell_currency,buy_currency);
    //     int256 lagTTokenABalance = tPools[tradePoolIndex].tokenABalance;

    //     //lag_T(buy_currency,sell_currency,sell_currency)=T(buy_currency,sell_currency,sell_currency);
    //     int256 lagTTokenBBalance = tPools[tradePoolIndex].tokenBBalance;

    //     //%substract amount and add fees to amount_in
    //     //T(buy_currency,sell_currency,buy_currency)=lag_T(buy_currency,sell_currency,buy_currency)-Buy*(1-fee_T(buy_currency,sell_currency)); ****
    //     tPools[tradePoolIndex].tokenABalance =
    //         lagTTokenABalance -
    //         (amount - ((tPools[tradePoolIndex].fee * amount) / 1 ether));

    //     // T(buy_currency,sell_currency,sell_currency)=lag_T(buy_currency,sell_currency,buy_currency)*lag_T(buy_currency,sell_currency,sell_currency)/(lag_T(buy_currency,sell_currency,buy_currency)-Buy); // %calculate amount_out
    //     tPools[tradePoolIndex].tokenBBalance = tPools[tradePoolIndex]
    //         .tokenBBalance =
    //         (lagTTokenABalance * lagTTokenBBalance) /
    //         (lagTTokenABalance - amount);

    //     // for k=1:number_currencies
    //     for (uint256 k = 0; k < tokens.length; k++) {
    //         if (buy_currency == tokens[k]) continue;

    //         uint256 buy_k_poolIndex = getPoolIndex(
    //             rPools,
    //             buy_currency,
    //             tokens[k]
    //         );

    //         //lag_R(buy_currency,k,buy_currency)=R(buy_currency,k,buy_currency);
    //         lagR[buy_k_poolIndex].tokenABalance = rPools[buy_k_poolIndex]
    //             .tokenABalance;

    //         //lag_R(buy_currency,k,sell_currency)=R(buy_currency,k,sell_currency);
    //         lagR[buy_k_poolIndex].tokenBBalance = rPools[buy_k_poolIndex]
    //             .tokenBBalance;
    //     }

    //     // %take buy_currency proportional from real and virtual pool
    //     /*  R(buy_currency,sell_currency,buy_currency)=
    //                    lag_R(buy_currency,sell_currency,buy_currency) *
    //                    T(buy_currency,sell_currency,buy_currency)/
    //                    lag_T(buy_currency,sell_currency,buy_currency); */

    //     rPools[tradePoolIndex].tokenABalance =
    //         (lagR[tradePoolIndex].tokenABalance *
    //             tPools[tradePoolIndex].tokenABalance) /
    //         lagTTokenABalance;

    //     // %take sell_currency proportional from real and virtual pool
    //     /* R(buy_currency,sell_currency,sell_currency)=
    //         lag_R(buy_currency,sell_currency,sell_currency)*
    //         T(buy_currency,sell_currency,sell_currency)/
    //         lag_T(buy_currency,sell_currency,sell_currency);*/

    //     rPools[tradePoolIndex].tokenBBalance =
    //         (lagR[tradePoolIndex].tokenBBalance *
    //             tPools[tradePoolIndex].tokenBBalance) /
    //         lagTTokenBBalance;

    //     // %fill reverse
    //     // R(sell_currency,buy_currency,buy_currency)=R(buy_currency,sell_currency,buy_currency);
    //     rPools[reverseTradePoolIndex].tokenBBalance = rPools[tradePoolIndex]
    //         .tokenABalance;

    //     // R(sell_currency,buy_currency,sell_currency)=R(buy_currency,sell_currency,sell_currency);
    //     rPools[reverseTradePoolIndex].tokenABalance = rPools[tradePoolIndex]
    //         .tokenBBalance;

    //     //% Updating of non-native pools that contribute to BC virtual pool;
    //     for (uint256 k = 0; k < tokens.length; k++) {
    //         if (
    //             buy_currency == tokens[k] ||
    //             sell_currency == tokens[k]
    //         ) continue;

    //         uint256 buy_k_poolIndex = getPoolIndex(
    //             rPools,
    //             buy_currency,
    //             tokens[k]
    //         );

    //         uint256 k_buy_poolIndex = getPoolIndex(
    //             rPools,
    //             tokens[k],
    //             buy_currency
    //         );

    //         //sum lagR tokenA balance
    //         int256 summ = 0;
    //         for (uint256 z = 0; z < lagR.length; z++) {
    //             if (lagR[z].tokenABalance > 0) {
    //                 summ += lagR[z].tokenABalance;
    //             }
    //         }

    //         /*R(buy_currency,k,buy_currency)=
    //             //R(buy_currency,k,buy_currency)+
    //             ((T(buy_currency,sell_currency,buy_currency)-lag_T(buy_currency,sell_currency,buy_currency))-
    //             (R(buy_currency,sell_currency,buy_currency)-lag_R(buy_currency,sell_currency,buy_currency)))*
    //             lag_R(buy_currency,k,buy_currency)/
    //             (sum(lag_R(buy_currency,1:number_currencies,buy_currency))
    //             -lag_R(buy_currency,sell_currency,buy_currency));
    // */
    //         rPools[buy_k_poolIndex].tokenABalance =
    //             rPools[buy_k_poolIndex].tokenABalance +
    //             (((tPools[tradePoolIndex].tokenABalance - lagTTokenABalance) -
    //                 (rPools[tradePoolIndex].tokenABalance -
    //                     lagR[tradePoolIndex].tokenABalance)) *
    //                 lagR[buy_k_poolIndex].tokenABalance) /
    //             (summ - lagR[tradePoolIndex].tokenABalance);

    //         //fill reverse pool
    //         //R(k,buy_currency,buy_currency)=R(buy_currency,k,buy_currency);
    //         rPools[k_buy_poolIndex].tokenBBalance = rPools[buy_k_poolIndex]
    //             .tokenABalance;
    //     }

    //     // % Updating reserves of real pools and all the subsequent calculations;
    //     // i=buy_currency;
    //     // for k=1:number_currencies
    //     //     if (k~=buy_currency & k~=sell_currency)
    //     //         R(buy_currency,k,sell_currency)=R(buy_currency,k,sell_currency)+((T(buy_currency,sell_currency,sell_currency)-lag_T(buy_currency,sell_currency,sell_currency))-(R(buy_currency,sell_currency,sell_currency)-lag_R(buy_currency,sell_currency,sell_currency)))*lag_R(buy_currency,k,buy_currency)/(sum(lag_R(buy_currency,1:number_currencies,buy_currency))-lag_R(buy_currency,sell_currency,buy_currency));
    //     //         R(k,buy_currency,sell_currency)=R(buy_currency,k,sell_currency);
    //     //     end;
    //     // end;
    //     for (uint256 k = 0; k < tokens.length; k++) {
    //         if (
    //             buy_currency == tokens[k] ||
    //             sell_currency == tokens[k]
    //         ) continue;

    //         uint256 buy_k_poolIndex = getPoolIndex(
    //             rPools,
    //             buy_currency,
    //             tokens[k]
    //         );

    //         uint256 k_buy_poolIndex = getPoolIndex(
    //             rPools,
    //             tokens[k],
    //             buy_currency
    //         );

    //         //sum lagR tokenA balance
    //         int256 summ = 0;
    //         for (uint256 z = 0; z < lagR.length; z++) {
    //             if (lagR[z].tokenABalance > 0) {
    //                 summ += lagR[z].tokenABalance;
    //             }
    //         }

    //         /*R(buy_currency,k,sell_currency)=
    //             R(buy_currency,k,sell_currency)+
    //             ((T(buy_currency,sell_currency,sell_currency)-lag_T(buy_currency,sell_currency,sell_currency))-
    //             (R(buy_currency,sell_currency,sell_currency)-lag_R(buy_currency,sell_currency,sell_currency)))*
    //             lag_R(buy_currency,k,buy_currency)/(sum(lag_R(buy_currency,1:number_currencies,buy_currency))-
    //             lag_R(buy_currency,sell_currency,buy_currency));*/

    //         rPools[buy_k_poolIndex].reserves[sell_currency].reserveBalance =
    //             rPools[buy_k_poolIndex].reserves[sell_currency].reserveBalance +
    //             (((tPools[tradePoolIndex].tokenBBalance - lagTTokenBBalance) -
    //                 (rPools[tradePoolIndex].tokenBBalance -
    //                     lagR[tradePoolIndex].tokenBBalance)) *
    //                 lagR[buy_k_poolIndex].tokenABalance) /
    //             (summ - lagR[tradePoolIndex].tokenABalance);

    //         //fill reverse pool
    //         rPools[k_buy_poolIndex]
    //             .reserves[sell_currency]
    //             .reserveBalance = rPools[buy_k_poolIndex]
    //             .reserves[sell_currency]
    //             .reserveBalance;
    //     }
    // }

    function exchageReserves(Pool[] storage rPools, address[] memory tokens)
        public
    {
        for (uint256 i = 0; i < rPools.length; i++) {
            for (uint256 k = 0; k < tokens.length; k++) {
                if (
                    rPools[i].tokenA == tokens[k] ||
                    rPools[i].tokenB == tokens[k]
                ) continue;

                //ij - rPools[i]

                //ik
                uint256 ikIndex = getPoolIndex(
                    rPools,
                    rPools[i].tokenA,
                    tokens[k]
                );

                //if (k~=i & k~=j & R(i,j,k)>0 & R(i,k,j)>0)
                if (
                    rPools[i].reserves[tokens[k]].reserveBalance > 0 &&
                    rPools[ikIndex].reserves[rPools[i].tokenB].reserveBalance >
                    0
                ) {
                    //lag_R(i,j,k)=R(i,j,k);
                    int256 lagRIJK = rPools[i]
                        .reserves[tokens[k]]
                        .reserveBalance;

                    //lag_R(i,k,j)=R(i,k,j);
                    int256 lagRIKJ = rPools[ikIndex]
                        .reserves[rPools[i].tokenB]
                        .reserveBalance;

                    //lag_R(i,j,j)=R(i,j,j);
                    int256 lagRIJI = rPools[i].tokenABalance;

                    //lag_R(i,j,j)=R(i,j,j);
                    int256 lagRIJJ = rPools[i].tokenBBalance;

                    //lag_R(i,k,k)=R(i,k,k);
                    int256 lagRIKK = rPools[ikIndex].tokenBBalance;

                    //lag_R(i,k,I)=R(i,k,k);
                    int256 lagRIKI = rPools[ikIndex].tokenABalance;

                    //R(i,j,k)=lag_R(i,j,k)-min(lag_R(i,j,k),lag_R(i,k,j)*lag_R(i,k,k)/lag_R(i,k,i)*lag_R(i,j,i)/lag_R(i,j,j));
                    rPools[i].reserves[tokens[k]].reserveBalance =
                        lagRIJK -
                        min(
                            lagRIJK,
                            ((((lagRIKJ * lagRIKK) / lagRIKI) * lagRIJI) /
                                lagRIJJ)
                        );

                    //R(i,k,j)=lag_R(i,k,j)-min(lag_R(i,k,j),lag_R(i,j,k)*lag_R(i,j,j)/lag_R(i,j,i)*lag_R(i,k,i)/lag_R(i,k,k));
                    rPools[ikIndex].reserves[rPools[i].tokenB].reserveBalance =
                        lagRIKJ -
                        min(
                            lagRIKJ,
                            ((((lagRIJK * lagRIJJ) / lagRIJI) * lagRIKI) /
                                lagRIKK)
                        );

                    //R(i,j,j)=lag_R(i,j,j)+lag_R(i,k,j)-R(i,k,j);
                    rPools[i].tokenBBalance =
                        lagRIJJ +
                        lagRIKJ -
                        rPools[ikIndex]
                            .reserves[rPools[i].tokenB]
                            .reserveBalance;

                    //R(i,k,k)=lag_R(i,k,k)+lag_R(i,j,k)-R(i,j,k);
                    rPools[ikIndex].tokenBBalance =
                        lagRIKK +
                        lagRIJK -
                        rPools[i].reserves[tokens[k]].reserveBalance;

                    uint256 jiIndex = getPoolIndex(
                        rPools,
                        rPools[i].tokenB,
                        rPools[i].tokenA
                    );
                    //R(j,i,k)=R(i,j,k);
                    rPools[jiIndex].reserves[tokens[k]].reserveBalance = rPools[
                        i
                    ].reserves[tokens[k]].reserveBalance;

                    uint256 kiIndex = getPoolIndex(
                        rPools,
                        tokens[k],
                        rPools[i].tokenA
                    );

                    //R(k,i,j)=R(i,k,j);
                    rPools[kiIndex]
                        .reserves[rPools[i].tokenB]
                        .reserveBalance = rPools[ikIndex]
                        .reserves[rPools[i].tokenB]
                        .reserveBalance;

                    //R(j,i,j)=R(i,j,j);
                    rPools[jiIndex].tokenABalance = rPools[i].tokenBBalance;

                    //R(k,i,k)=R(i,k,k);
                    rPools[kiIndex].tokenABalance = rPools[kiIndex]
                        .tokenBBalance;
                }
            }
        }
    }

    //
}

contract vPool {
    uint256 public constant MINIMUM_LIQUIDITY = 10**3;

    function getTotalsPool() public view returns (VirtualPoolVM[] memory) {
        return vPoolCalculations.getTotalsPool(rPools, _tokens);
    }

    // function _calculateVirtualPools()
    //     public
    //     view
    //     returns (VirtualPool[] memory)
    // {
    //     return vPoolCalculations._calculateVirtualPools(rPools, _tokens);
    // }

    function testFunction1() public returns (VirtualPool[] memory result) {
        VirtualPool[] memory vPools = new VirtualPool[](rPools.length);
        int256[] memory belowReserve = vPoolCalculations._calculateBelowThreshold(rPools, _tokens);
        emit UDebug("here", 0);
        for (uint256 i = 0; i < rPools.length; i++) {
            for (uint256 k = 0; k < _tokens.length; k++) {
                if (
                    rPools[i].tokenA == _tokens[k] ||
                    rPools[i].tokenB == _tokens[k]
                ) continue;

                // emit ADebug("rPools[i].tokenA", rPools[i].tokenA);
                // emit ADebug("_tokens[k]", _tokens[k]);
                uint256 ikIndex = vPoolCalculations.getPoolIndex(
                    rPools,
                    rPools[i].tokenA,
                    _tokens[k]
                );

                uint256 jkIndex = vPoolCalculations.getPoolIndex(
                    rPools,
                    rPools[i].tokenB,
                    _tokens[k]
                );

                // int256 ikIndexTokenABalance = 0;
                // int256 ikIndexTokenBBalance = 0;
                // int256 jkIndexTokenABalance = 0;
                // int256 jkIndexTokenBBalance = 0;
                // vPools[i].fee = 0.003 ether;
                emit UDebug("ikIndex", ikIndex);
                emit UDebug("jkIndex", jkIndex);
            }
        }
    }

    function _calculateVirtualPools() public returns (VirtualPool[] memory result) {
        VirtualPool[] memory vPools = new VirtualPool[](rPools.length);
        // int256[] memory belowReserve = vPoolCalculations._calculateBelowThreshold(rPools, _tokens);
        emit UDebug("here", 0);
        for (uint256 i = 0; i < rPools.length; i++) {
            for (uint256 k = 0; k < _tokens.length; k++) {
                if (
                    rPools[i].tokenA == _tokens[k] ||
                    rPools[i].tokenB == _tokens[k]
                ) continue;

                // emit ADebug("rPools[i].tokenA", rPools[i].tokenA);
                // emit ADebug("_tokens[k]", _tokens[k]);
                uint256 ikIndex = vPoolCalculations.getPoolIndex(
                    rPools,
                    rPools[i].tokenA,
                    _tokens[k]
                );

                uint256 jkIndex = vPoolCalculations.getPoolIndex(
                    rPools,
                    rPools[i].tokenB,
                    _tokens[k]
                );

                // int256 ikIndexTokenABalance = 0;
                // int256 ikIndexTokenBBalance = 0;
                // int256 jkIndexTokenABalance = 0;
                // int256 jkIndexTokenBBalance = 0;
                // vPools[i].fee = 0.003 ether;
                emit UDebug("ikIndex", ikIndex);
                emit UDebug("jkIndex", jkIndex);
                // if (ikIndex == 999 || jkIndex == 999) {
                //     continue;
                // }

                // if (ikIndex < 999) {
                //     ikIndexTokenABalance = rPools[ikIndex].tokenABalance;
                //     ikIndexTokenBBalance = rPools[ikIndex].tokenBBalance;
                // }
                // if (jkIndex < 999) {
                //     jkIndexTokenABalance = rPools[jkIndex].tokenABalance;
                //     jkIndexTokenBBalance = rPools[jkIndex].tokenBBalance;
                // }

                // emit Debug("ikIndexTokenABalance", ikIndexTokenABalance);
                // emit Debug("ikIndexTokenBBalance", ikIndexTokenBBalance);

                // emit Debug("jkIndexTokenABalance", jkIndexTokenABalance);
                // emit Debug("jkIndexTokenBBalance", jkIndexTokenBBalance);

                // if () {

                //     vPools[i].tokenBBalance = 0;
                // }

                // vPools[i].tokenAName = rPools[i].tokenA.name;
                // vPools[i].tokenBName = rPools[i].tokenB.name;
                //  V(i,j,i)=V(i,j,i)+ind_below_reserve_threshold(i,k)*R(i,k,i)*min(R(i,k,k),R(j,k,k))/max(R(i,k,k),epsilon);

                // emit Debug("Here", 0);
                // vPools[i].tokenABalance =
                //     vPools[i].tokenABalance +
                //     (belowReserve[i] *
                //         ikIndexTokenABalance *
                //         vPoolCalculations.min(
                //             ikIndexTokenBBalance,
                //             jkIndexTokenBBalance
                //         )) /
                //     vPoolCalculations.max(ikIndexTokenBBalance, epsilon);

                // //  V(i,j,j)=V(i,j,j)+ind_below_reserve_threshold(i,k)*R(j,k,j)*min(R(i,k,k),R(j,k,k))/max(R(j,k,k),epsilon);
                // vPools[i].tokenBBalance =
                //     vPools[i].tokenBBalance +
                //     (belowReserve[i] *
                //         jkIndexTokenABalance *
                //         min(ikIndexTokenBBalance, jkIndexTokenBBalance)) /
                //     max(jkIndexTokenBBalance, epsilon);
            }

            // int256 imbalanceRatio = (vPools[i].tokenABalance /
            //     vPools[i].tokenBBalance) /
            //     (rPools[i].tokenABalance / rPools[i].tokenBBalance);

            // vPools[i].balanced =
            //     (imbalanceRatio > 1 + imbalance_tolerance_base) ||
            //     (imbalanceRatio < 1 - imbalance_tolerance_base);
        }

        // return vPools;
    }

    function _calculateReserveRatio() public view returns (int256[] memory) {
        return vPoolCalculations._calculateReserveRatio(rPools, _tokens);
    }

    function _calculateBelowThreshold() public view returns (int256[] memory) {
        return vPoolCalculations._calculateBelowThreshold(rPools, _tokens);
    }

    function getRPools() public view returns (PoolVM[] memory) {
        PoolVM[] memory temp = new PoolVM[](rPools.length);

        // int256[] memory reserveRatio = _calculateReserveRatio();
        // int256[] memory belowReserve = _calculateBelowThreshold();

        for (uint256 i = 0; i < rPools.length; i++) {
            temp[i].tokenA = rPools[i].tokenA;
            temp[i].tokenB = rPools[i].tokenB;
            temp[i].LPToken = rPools[i].LPToken;
            temp[i].fee = rPools[i].fee;
            // temp[i].reserveRatio = reserveRatio[i];
            //   temp[i].belowReserve = belowReserve[i];
            temp[i].tokenABalance = rPools[i].tokenABalance;
            temp[i].tokenBBalance = rPools[i].tokenBBalance;
            temp[i].maxReserveRatio = rPools[i].maxReserveRatio;
        }
        return temp;
    }

    function getVPools() public returns (VirtualPoolVM[] memory) {
        VirtualPool[] memory vPools = _calculateVirtualPools();
        VirtualPoolVM[] memory temp = new VirtualPoolVM[](vPools.length);

        for (uint256 i = 0; i < vPools.length; i++) {
            temp[i].fee = vPools[i].fee;
            temp[i].tokenABalance = vPools[i].tokenABalance;
            temp[i].tokenBBalance = vPools[i].tokenBBalance;
            temp[i].tokenAName = vPools[i].tokenAName;
            temp[i].tokenBName = vPools[i].tokenBName;
        }
        return temp;
    }

    event Debug(string message, int256 value);

    event UDebug(string message, uint256 value);

    event ADebug(string message, address value);

    int256 epsilon = 1 wei;
    int256 imbalance_tolerance_base = 0.01 ether;

    Pool[] rPools;
    address[] _tokens;

    address owner;

    constructor() {
        owner = msg.sender;
    }

    function createPool(address tokenA, address tokenB) internal {
        rPools.push();
        Pool storage newPool = rPools[rPools.length - 1];
        newPool.tokenA = tokenA;
        newPool.tokenB = tokenB;
        newPool.fee = 0.002 ether;
        newPool.maxReserveRatio = 0.02 ether;

        if (!vPoolCalculations.tokenExist(_tokens, tokenA)) {
            _tokens.push(tokenA);
        }

        if (!vPoolCalculations.tokenExist(_tokens, tokenB)) {
            _tokens.push(tokenB);
        }

        //create LP token
        string memory name = string(abi.encodePacked("tt-LP", rPools.length));

        ERC20 LPToken = new ERC20(name, "VRSWP-LP");
        newPool.LPToken = address(LPToken);

        //mint first tokens to blackhole
        //    LPToken._mint(address(0), MINIMUM_LIQUIDITY);
    }

    function emptyLiquidity(address tokenAAddress, address tokenBAddress)
        public
    {
        uint256 poolIndex = vPoolCalculations.getPoolIndex(
            rPools,
            tokenAAddress,
            tokenBAddress
        );

        if (msg.sender == owner && poolIndex < 999) {
            require(
                IERC20(tokenAAddress).transfer(
                    owner,
                    uint256(rPools[poolIndex].tokenABalance)
                ),
                "Failed to send token A"
            );
            rPools[poolIndex].tokenABalance = 0;

            require(
                IERC20(tokenBAddress).transfer(
                    owner,
                    uint256(rPools[poolIndex].tokenBBalance)
                ),
                "Failed to send token B"
            );
            rPools[poolIndex].tokenBBalance = 0;
        }
    }

    function depositLiquidity(
        address tokenA,
        address tokenB,
        uint256 tokenAAmount,
        uint256 tokenBAmount
    ) public {
        IERC20 tokenAInstance = IERC20(tokenA);
        IERC20 tokenBInstance = IERC20(tokenB);

        uint256 poolIndex = vPoolCalculations.getPoolIndex(
            rPools,
            tokenA,
            tokenB
        );

        if (poolIndex == 999) {
            createPool(tokenA, tokenB);
        }

        poolIndex = vPoolCalculations.getPoolIndex(rPools, tokenA, tokenB);
        require(poolIndex != 999, "Could not find pool");

        require(
            tokenAInstance.transferFrom(
                msg.sender,
                address(this),
                tokenAAmount
            ),
            "Could not transfer token A"
        );
        require(
            tokenBInstance.transferFrom(
                msg.sender,
                address(this),
                tokenBAmount
            ),
            "Could not transfer token B"
        );

        rPools[poolIndex].tokenABalance =
            rPools[poolIndex].tokenABalance +
            int256(tokenAAmount);

        rPools[poolIndex].tokenBBalance =
            rPools[poolIndex].tokenBBalance +
            int256(tokenBAmount);

        //issue LP tokens
        ERC20(rPools[poolIndex].LPToken)._mint(msg.sender, tokenAAmount);
    }

    // function _initPools() public {
    //     if (_poolsInitialized) {
    //         emit Debug("Pools already initialized", 0);
    //         return;
    //     }

    //     for (uint256 i = 0; i < _tokens.length; i++) {
    //         for (uint256 j = 0; j < _tokens.length; j++) {
    //             if (i == j) continue;

    //             rPools.push();
    //             Pool storage newPool = rPools[rPools.length - 1];
    //             newPool.tokenA = _tokens[i].tokenAddress;
    //             newPool.tokenB = _tokens[j].tokenAddress;
    //             newPool.fee = 0.002 ether;
    //             newPool.tokenABalance = 0;
    //             newPool.tokenBBalance = 0;
    //             newPool.maxReserveRatio = 0.02 ether;

    //             ///init reserves
    //             for (uint256 k = 0; k < _tokens.length; k++) {
    //                 if (
    //                     _tokens[k].tokenAddress == _tokens[i].tokenAddress ||
    //                     _tokens[k].tokenAddress == _tokens[j].tokenAddress
    //                 ) continue;

    //                 newPool
    //                     .reserves[_tokens[k].tokenAddress]
    //                     .tokenAddress = _tokens[k].tokenAddress; // temporary for tests

    //                 newPool
    //                     .reserves[_tokens[k].tokenAddress]
    //                     .tokenName = _tokens[k].name; // temporary for tests

    //                 newPool
    //                     .reserves[_tokens[k].tokenAddress]
    //                     .reserveBalance = 0;
    //             }
    //         }
    //     }

    //     _poolsInitialized = true;
    //     emit Debug("Pools initialized", 0);

    //     _initLiquidityProvide();
    // }

    // function getRPoolTokenABalance(uint256 poolId)
    //     public
    //     view
    //     returns (int256)
    // {
    //     return rPools[poolId].tokenABalance;
    // }

    function getTokens() public view returns (address[] memory) {
        return _tokens;
    }

    function getPoolsCount() public view returns (uint256) {
        return rPools.length;
    }

    function getTotalPoolFee(address buy_currency, address sell_currency)
        public
        view
        returns (int256)
    {
        VirtualPoolVM[] memory tPools = vPoolCalculations.getTPools(
            rPools,
            _tokens
        );

        uint256 tradePoolIndex = vPoolCalculations.getPoolIndex(
            rPools,
            buy_currency,
            sell_currency
        );
        return tPools[tradePoolIndex].fee;
    }
}
