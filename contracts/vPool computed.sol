// // SPDX-License-Identifier: MIT
// pragma solidity >=0.4.22 <0.9.0;

// import "./Types256.sol";
// import "./Console.sol";

// library vMath {}

// library ViewModelBuilder {}

// contract vPoolA is Console {
//     function max(int256 a, int256 b) internal pure returns (int256) {
//         return a >= b ? a : b;
//     }

//     function min(int256 a, int256 b) internal pure returns (int256) {
//         return a < b ? a : b;
//     }

//     function getRPools() public view returns (PoolVM[] memory) {
//         PoolVM[] memory temp = new PoolVM[](rPools.length);

//         for (uint256 i = 0; i < rPools.length; i++) {
//             temp[i].tokenA = rPools[i].tokenA;
//             temp[i].tokenB = rPools[i].tokenB;
//             temp[i].fee = rPools[i].fee;
//             temp[i].reserveRatio = rPools[i].reserveRatio;
//             temp[i].belowReserve = rPools[i].belowReserve;
//             temp[i].tokenABalance = rPools[i].tokenABalance;
//             temp[i].tokenBBalance = rPools[i].tokenBBalance;
//             temp[i].maxReserveRatio = rPools[i].maxReserveRatio;
//         }
//         return temp;
//     }

//     function getVPools() public view returns (VirtualPoolVM[] memory) {
//         VirtualPoolVM[] memory temp = new VirtualPoolVM[](vPools.length);

//         for (uint256 i = 0; i < vPools.length; i++) {
//             temp[i].fee = vPools[i].fee;
//             temp[i].tokenABalance = vPools[i].tokenABalance;
//             temp[i].tokenBBalance = vPools[i].tokenBBalance;
//             temp[i].tokenAName = vPools[i].tokenAName;
//             temp[i].tokenBName = vPools[i].tokenBName;
//         }
//         return temp;
//     }

//     function uint2str(uint256 _i)
//         internal
//         pure
//         returns (string memory _uintAsString)
//     {
//         if (_i == 0) {
//             return "0";
//         }
//         uint256 j = _i;
//         uint256 len;
//         while (j != 0) {
//             len++;
//             j /= 10;
//         }
//         bytes memory bstr = new bytes(len);
//         uint256 k = len;
//         while (_i != 0) {
//             k = k - 1;
//             uint8 temp = (48 + uint8(_i - (_i / 10) * 10));
//             bytes1 b1 = bytes1(temp);
//             bstr[k] = b1;
//             _i /= 10;
//         }
//         return string(bstr);
//     }

//     event Debug(string message);

//     bool _poolsInitialized;

//     Pool invalidPool;

//     int256 epsilon = 1 wei;

//     Pool[] rPools;

//     VirtualPool[] vPools;

//     Token token1;
//     Token token2;
//     Token token3;
//     Token token4;

//     Token[] _tokens;

//     constructor() {
//         token1 = Token({
//             tokenAddress: address(0x8d6a3aB7332557878a94757b1F4F2f30B7be4f38),
//             price: 100 ether,
//             name: "A"
//         });
//         token2 = Token({
//             tokenAddress: address(0x801a666431Ac9e2515634e3A1Beb1f7932112921),
//             price: 50 ether,
//             name: "B"
//         });
//         token3 = Token({
//             tokenAddress: address(0x9Ae29319e97dA618382A87E367c8e7E7a96FAd81),
//             price: 150 ether,
//             name: "C"
//         });
//         token4 = Token({
//             tokenAddress: address(0xa589332749DB4b0181FeBB4B2Dfcd81410a7D8e6),
//             price: 70 ether,
//             name: "D"
//         });

//         _tokens.push(token1);
//         _tokens.push(token2);
//         _tokens.push(token3);
//         _tokens.push(token4);

//         _poolsInitialized = false;
//     }

//     function _initPools() public {
//         if (_poolsInitialized) {
//             emit Debug("Pools already initialized");
//             return;
//         }

//         for (uint256 i = 0; i < _tokens.length; i++) {
//             for (uint256 j = 0; j < _tokens.length; j++) {
//                 if (i == j) continue;

//                 rPools.push();
//                 Pool storage newPool = rPools[rPools.length - 1];
//                 newPool.tokenA = _tokens[i];
//                 newPool.tokenB = _tokens[j];
//                 newPool.fee = 0.002 ether;
//                 newPool.reserveRatio = 0;
//                 newPool.belowReserve = 1;
//                 newPool.tokenABalance = 0;
//                 newPool.tokenBBalance = 0;
//                 newPool.maxReserveRatio = 0.02 ether;

//                 vPools.push();
//                 VirtualPool storage newVPool = vPools[vPools.length - 1];
//                 newVPool.tokenAName = newPool.tokenA.name;
//                 newVPool.tokenBName = newPool.tokenB.name;
//                 newVPool.fee = 0.003 ether;

//                 ///init reserves
//                 for (uint256 k = 0; k < _tokens.length; k++) {
//                     if (
//                         _tokens[k].tokenAddress == _tokens[i].tokenAddress ||
//                         _tokens[k].tokenAddress == _tokens[j].tokenAddress
//                     ) continue;

//                     newPool
//                         .reserves[_tokens[k].tokenAddress]
//                         .tokenAddress = _tokens[k].tokenAddress; // temporary for tests

//                     newPool
//                         .reserves[_tokens[k].tokenAddress]
//                         .tokenName = _tokens[k].name; // temporary for tests

//                     newPool
//                         .reserves[_tokens[k].tokenAddress]
//                         .reserveBalance = 0;
//                 }
//             }
//         }

//         _poolsInitialized = true;
//         emit Debug("Pools initialized");

//         _initLiquidityProvide();
//     }

//     function _initLiquidityProvide() public {
//         require(_poolsInitialized, "Pools not initialized");

//         rPools[0].tokenABalance = 100 ether;
//         rPools[1].tokenABalance = 130 ether;
//         rPools[2].tokenABalance = 30 ether;
//         rPools[3].tokenABalance = 200 ether;
//         rPools[4].tokenABalance = 0 ether;
//         rPools[5].tokenABalance = 600 ether;

//         rPools[6].tokenABalance = 87 ether;
//         rPools[7].tokenABalance = 0 ether;
//         rPools[8].tokenABalance = 150 ether;
//         rPools[9].tokenABalance = 43 ether;
//         rPools[10].tokenABalance = 429 ether;
//         rPools[11].tokenABalance = 322 ether;

//         //calcilate asset B balance
//         for (uint256 i = 0; i < rPools.length; i++) {
//             rPools[i].tokenBBalance =
//                 (rPools[i].tokenABalance * rPools[i].tokenA.price) /
//                 rPools[i].tokenB.price;
//         }

//         emit Debug("liquidity providers initialized");
//     }

//     function getPoolIndex(address tokenA, address tokenB)
//         internal
//         view
//         returns (uint256)
//     {
//         uint256 index = 0;
//         bool found = false;
//         for (uint256 i = 0; i < rPools.length; i++) {
//             if (
//                 rPools[i].tokenA.tokenAddress == tokenA &&
//                 rPools[i].tokenB.tokenAddress == tokenB
//             ) {
//                 index = i;
//                 found = true;
//                 break;
//             }
//         }

//         return index;
//     }

//     function _calculateReserveRatio() public {
//         require(_poolsInitialized, "Pools not initialized");

//         for (uint256 i = 0; i < rPools.length; i++) {
//             for (uint256 k = 0; k < _tokens.length; k++) {
//                 if (
//                     rPools[i].tokenA.tokenAddress == _tokens[k].tokenAddress ||
//                     rPools[i].tokenB.tokenAddress == _tokens[k].tokenAddress
//                 ) continue;

//                 uint256 ikIndex = getPoolIndex(
//                     rPools[i].tokenA.tokenAddress,
//                     _tokens[k].tokenAddress
//                 );

//                 uint256 jkIndex = getPoolIndex(
//                     rPools[i].tokenB.tokenAddress,
//                     _tokens[k].tokenAddress
//                 );

//                 rPools[i].reserveRatio = 0;
//                 rPools[i].reserveRatio =
//                     rPools[i].reserveRatio +
//                     (rPools[i]
//                         .reserves[_tokens[k].tokenAddress]
//                         .reserveBalance *
//                         max(
//                             rPools[ikIndex].tokenABalance /
//                                 max(rPools[ikIndex].tokenBBalance, epsilon),
//                             ((rPools[jkIndex].tokenABalance /
//                                 max(rPools[jkIndex].tokenBBalance, epsilon)) *
//                                 rPools[i].tokenABalance) /
//                                 max(rPools[i].tokenBBalance, epsilon)
//                         )) /
//                     (2 * max(rPools[i].tokenABalance, epsilon));
//             }
//         }

//         emit Debug("Calculate reserve ratio ended");
//     }

//     function _calculateBelowThreshold() public {
//         for (uint256 i = 0; i < rPools.length; i++) {
//             if (
//                 rPools[i].reserveRatio >= rPools[i].maxReserveRatio &&
//                 rPools[i].belowReserve == 1
//             ) {
//                 rPools[i].belowReserve = 0;
//             } else if (rPools[i].belowReserve == 0) {
//                 rPools[i].belowReserve = 1;
//             }
//         }

//         emit Debug("Calculate threshold ended");
//     }

//     function getTotalsPool() public view returns (VirtualPoolVM[] memory) {
//         VirtualPoolVM[] memory temp = new VirtualPoolVM[](rPools.length);
//         for (uint256 i = 0; i < rPools.length; i++) {
//             temp[i].fee = vPools[i].fee;

//             temp[i].tokenABalance =
//                 rPools[i].tokenABalance +
//                 vPools[i].tokenABalance;

//             temp[i].tokenBBalance =
//                 rPools[i].tokenBBalance +
//                 vPools[i].tokenBBalance;

//             temp[i].tokenAName = vPools[i].tokenAName;
//             temp[i].tokenBName = vPools[i].tokenBName;
//         }
//         return temp;
//     }

//     function _calculateVirtualPools() public {
//         for (uint256 i = 0; i < vPools.length; i++) {
//             vPools[i].tokenABalance = 0;
//             vPools[i].tokenBBalance = 0;
//         }

//         for (uint256 i = 0; i < rPools.length; i++) {
//             for (uint256 k = 0; k < _tokens.length; k++) {
//                 if (
//                     rPools[i].tokenA.tokenAddress == _tokens[k].tokenAddress ||
//                     rPools[i].tokenB.tokenAddress == _tokens[k].tokenAddress
//                 ) continue;

//                 uint256 ikIndex = getPoolIndex(
//                     rPools[i].tokenA.tokenAddress,
//                     _tokens[k].tokenAddress
//                 );

//                 uint256 jkIndex = getPoolIndex(
//                     rPools[i].tokenB.tokenAddress,
//                     _tokens[k].tokenAddress
//                 );

//                 VirtualPool storage currVpool = vPools[i];

//                 //  V(i,j,i)=V(i,j,i)+ind_below_reserve_threshold(i,k)*R(i,k,i)*min(R(i,k,k),R(j,k,k))/max(R(i,k,k),epsilon);
//                 currVpool.tokenABalance =
//                     currVpool.tokenABalance +
//                     (rPools[ikIndex].belowReserve *
//                         rPools[ikIndex].tokenABalance *
//                         min(
//                             rPools[ikIndex].tokenBBalance,
//                             rPools[jkIndex].tokenBBalance
//                         )) /
//                     max(rPools[ikIndex].tokenBBalance, epsilon);

//                 //  V(i,j,j)=V(i,j,j)+ind_below_reserve_threshold(i,k)*R(j,k,j)*min(R(i,k,k),R(j,k,k))/max(R(j,k,k),epsilon);
//                 currVpool.tokenBBalance =
//                     currVpool.tokenBBalance +
//                     (rPools[ikIndex].belowReserve *
//                         rPools[jkIndex].tokenABalance *
//                         min(
//                             rPools[ikIndex].tokenBBalance,
//                             rPools[jkIndex].tokenBBalance
//                         )) /
//                     max(rPools[jkIndex].tokenBBalance, epsilon);
//             }
//         }

//         Console.log("Calculate virtual pools ended");
//     }

//     function getTokens() public view returns (Token[] memory) {
//         return _tokens;
//     }

//     function getPoolsInitialized() public view returns (bool) {
//         return _poolsInitialized;
//     }

//     function getPoolsCount() public view returns (uint256) {
//         return rPools.length;
//     }

//     function getTPools() public view returns (VirtualPoolVM[] memory) {
//         VirtualPoolVM[] memory temp = new VirtualPoolVM[](rPools.length);
//         for (uint256 i = 0; i < rPools.length; i++) {
//             temp[i].fee = vPools[i].fee;

//             temp[i].tokenABalance =
//                 rPools[i].tokenABalance +
//                 vPools[i].tokenABalance;

//             temp[i].tokenBBalance =
//                 rPools[i].tokenBBalance +
//                 vPools[i].tokenBBalance;

//             temp[i].tokenAName = vPools[i].tokenAName;
//             temp[i].tokenBName = vPools[i].tokenBName;

//             if (temp[i].tokenABalance > 0) {
//                 temp[i].fee =
//                     (rPools[i].fee *
//                         rPools[i].tokenABalance +
//                         vPools[i].fee *
//                         vPools[i].tokenABalance) /
//                     temp[i].tokenABalance;
//             }
//         }
//         return temp;
//     }

//     function getPoolReserve(address tokenA, address tokenB)
//         public
//         view
//         returns (ReserveBalance[] memory)
//     {
//         ReserveBalance[] memory temp = new ReserveBalance[](_tokens.length);
//         uint256 poolIndex = getPoolIndex(tokenA, tokenB);
//         temp[0] = rPools[poolIndex].reserves[_tokens[0].tokenAddress];
//         temp[1] = rPools[poolIndex].reserves[_tokens[1].tokenAddress];
//         temp[2] = rPools[poolIndex].reserves[_tokens[2].tokenAddress];
//         temp[3] = rPools[poolIndex].reserves[_tokens[3].tokenAddress];

//         return temp;
//     }

//     function swap(
//         address buy_currency,
//         address sell_currency,
//         int256 amount
//     ) public {
//         // buy_currency=2;
//         // sell_currency=3;
//         // Buy=30;

//         VirtualPoolVM[] memory tPools = getTPools();
//         VirtualPoolVM[] memory lagR = new VirtualPoolVM[](rPools.length);

//         uint256 tradePoolIndex = getPoolIndex(buy_currency, sell_currency);

//         uint256 reverseTradePoolIndex = getPoolIndex(
//             sell_currency,
//             buy_currency
//         );

//         // %save current values

//         //lag_T(buy_currency,sell_currency,buy_currency)=T(buy_currency,sell_currency,buy_currency);
//         int256 lagTTokenABalance = tPools[tradePoolIndex].tokenABalance;

//         //lag_T(buy_currency,sell_currency,sell_currency)=T(buy_currency,sell_currency,sell_currency);
//         int256 lagTTokenBBalance = tPools[tradePoolIndex].tokenBBalance;

//         //%add fees to amount_in
//         //T(buy_currency,sell_currency,buy_currency)=lag_T(buy_currency,sell_currency,buy_currency)-Buy*(1-fee_T(buy_currency,sell_currency)); ****
//         tPools[tradePoolIndex].tokenABalance =
//             lagTTokenABalance -
//             (amount - ((tPools[tradePoolIndex].fee * amount) / 1 ether));

//         // %calculate amount_out
//         // T(buy_currency,sell_currency,sell_currency)=lag_T(buy_currency,sell_currency,buy_currency)*lag_T(buy_currency,sell_currency,sell_currency)/(lag_T(buy_currency,sell_currency,buy_currency)-Buy);
//         tPools[tradePoolIndex].tokenBBalance =
//             (lagTTokenABalance * lagTTokenBBalance) /
//             (lagTTokenABalance - amount);

//         // % Updating of BC real pool;
//         // for k=1:number_currencies
//         for (uint256 k = 0; k < _tokens.length; k++) {
//             if (buy_currency == _tokens[k].tokenAddress) continue;

//             uint256 buy_k_poolIndex = getPoolIndex(
//                 buy_currency,
//                 _tokens[k].tokenAddress
//             );

//             //lag_R(buy_currency,k,buy_currency)=R(buy_currency,k,buy_currency);
//             lagR[buy_k_poolIndex].tokenABalance = rPools[buy_k_poolIndex]
//                 .tokenABalance;

//             //lag_R(buy_currency,k,sell_currency)=R(buy_currency,k,sell_currency);
//             lagR[buy_k_poolIndex].tokenBBalance = rPools[buy_k_poolIndex].reserves[sell_currency].reserveBalance; ///####TODO
//         }

//         // %take buy_currency proportional from real and virtual pool
//         /*  R(buy_currency,sell_currency,buy_currency)=
//                    lag_R(buy_currency,sell_currency,buy_currency) *
//                    T(buy_currency,sell_currency,buy_currency)/
//                    lag_T(buy_currency,sell_currency,buy_currency); */
//         rPools[tradePoolIndex].tokenABalance =
//             (lagR[tradePoolIndex].tokenABalance *
//                 tPools[tradePoolIndex].tokenABalance) /
//             lagTTokenABalance;

//         // %take sell_currency proportional from real and virtual pool
//         /* R(buy_currency,sell_currency,sell_currency)=
//         lag_R(buy_currency,sell_currency,sell_currency)*
//         T(buy_currency,sell_currency,sell_currency)/
//         lag_T(buy_currency,sell_currency,sell_currency);*/

//         rPools[tradePoolIndex].tokenBBalance =
//             (lagR[tradePoolIndex].tokenBBalance *
//                 tPools[tradePoolIndex].tokenBBalance) /
//             lagTTokenBBalance;

//         // %fill reverse
//         // R(sell_currency,buy_currency,buy_currency)=R(buy_currency,sell_currency,buy_currency);
//         rPools[reverseTradePoolIndex].tokenBBalance = rPools[tradePoolIndex]
//             .tokenABalance;

//         // R(sell_currency,buy_currency,sell_currency)=R(buy_currency,sell_currency,sell_currency);
//         rPools[reverseTradePoolIndex].tokenABalance = rPools[tradePoolIndex]
//             .tokenBBalance;

//         //% Updating of non-native pools that contribute to BC virtual pool;
//         for (uint256 k = 0; k < _tokens.length; k++) {
//             if (
//                 buy_currency == _tokens[k].tokenAddress ||
//                 sell_currency == _tokens[k].tokenAddress
//             ) continue;

//             uint256 buy_k_poolIndex = getPoolIndex(
//                 buy_currency,
//                 _tokens[k].tokenAddress
//             );

//             uint256 k_buy_poolIndex = getPoolIndex(
//                 _tokens[k].tokenAddress,
//                 buy_currency
//             );

//             //sum lagR tokenA balance
//             int256 summ = 0;
//             for (uint256 z = 0; z < lagR.length; z++) {
//                 if (lagR[z].tokenABalance > 0) {
//                     summ += lagR[z].tokenABalance;
//                 }
//             }

//             Console.log("summ", summ);

//             rPools[buy_k_poolIndex].tokenABalance =
//                 rPools[buy_k_poolIndex].tokenABalance +
//                 (((tPools[tradePoolIndex].tokenABalance - lagTTokenABalance) -
//                     (rPools[tradePoolIndex].tokenABalance -
//                         lagR[tradePoolIndex].tokenABalance)) *
//                     lagR[buy_k_poolIndex].tokenABalance) /
//                 summ -
//                 lagR[tradePoolIndex].tokenABalance;

//             //fill reverse pool
//             //R(k,buy_currency,buy_currency)=R(buy_currency,k,buy_currency);
//             rPools[k_buy_poolIndex].tokenBBalance = rPools[buy_k_poolIndex]
//                 .tokenABalance;
//         }

//         // % Updating reserves of real pools and all the subsequent calculations;
//         // i=buy_currency;
//         // for k=1:number_currencies
//         //     if (k~=buy_currency & k~=sell_currency)
//         //         R(buy_currency,k,sell_currency)=R(buy_currency,k,sell_currency)+((T(buy_currency,sell_currency,sell_currency)-lag_T(buy_currency,sell_currency,sell_currency))-(R(buy_currency,sell_currency,sell_currency)-lag_R(buy_currency,sell_currency,sell_currency)))*lag_R(buy_currency,k,buy_currency)/(sum(lag_R(buy_currency,1:number_currencies,buy_currency))-lag_R(buy_currency,sell_currency,buy_currency));
//         //         R(k,buy_currency,sell_currency)=R(buy_currency,k,sell_currency);
//         //     end;
//         // end;
//         for (uint256 k = 0; k < _tokens.length; k++) {
//             if (
//                 buy_currency == _tokens[k].tokenAddress ||
//                 sell_currency == _tokens[k].tokenAddress
//             ) continue;

//             uint256 buy_k_poolIndex = getPoolIndex(
//                 buy_currency,
//                 _tokens[k].tokenAddress
//             );

//             uint256 k_buy_poolIndex = getPoolIndex(
//                 _tokens[k].tokenAddress,
//                 buy_currency
//             );

//             //sum lagR tokenA balance
//             int256 summ = 0;
//             for (uint256 z = 0; z < lagR.length; z++) {
//                 if (lagR[z].tokenABalance > 0) {
//                     summ += lagR[z].tokenABalance;
//                 }
//             }

//             rPools[buy_k_poolIndex].reserves[sell_currency].reserveBalance =
//                 rPools[buy_k_poolIndex].reserves[sell_currency].reserveBalance +
//                 (((tPools[tradePoolIndex].tokenBBalance - lagTTokenBBalance) -
//                     (rPools[tradePoolIndex].tokenBBalance -
//                         lagR[tradePoolIndex].tokenBBalance)) *
//                     lagR[buy_k_poolIndex].tokenABalance) /
//                 (summ - lagR[tradePoolIndex].tokenBBalance);

//             //fill reverse pool
//             rPools[k_buy_poolIndex]
//                 .reserves[sell_currency]
//                 .reserveBalance = rPools[buy_k_poolIndex]
//                 .reserves[sell_currency]
//                 .reserveBalance;
//         }

//         _calculateReserveRatio();
//         _calculateBelowThreshold();
//         _calculateVirtualPools();
//     }
// }
