// pragma solidity >=0.4.22 <0.9.0;
// import "./Types256.sol";
// import "./Types128.sol";

// library ViewModelBuilder {
//     function getRPools() public view returns (PoolVM[] memory) {
//         PoolVM[] memory temp = new PoolVM[](rPools.length);

//         int256[] memory reserveRatio = _calculateReserveRatio();
//         int256[] memory belowReserve = _calculateBelowThreshold();

//         for (uint256 i = 0; i < rPools.length; i++) {
//             temp[i].tokenA = rPools[i].tokenA;
//             temp[i].tokenB = rPools[i].tokenB;
//             temp[i].fee = rPools[i].fee;
//             temp[i].reserveRatio = reserveRatio[i];
//             temp[i].belowReserve = belowReserve[i];
//             temp[i].tokenABalance = rPools[i].tokenABalance;
//             temp[i].tokenBBalance = rPools[i].tokenBBalance;
//             temp[i].maxReserveRatio = rPools[i].maxReserveRatio;
//         }
//         return temp;
//     }

//     function getVPools(VirtualPool[] storage vPools)
//         public
//         view
//         returns (VirtualPoolVM[] memory)
//     {
//         VirtualPoolVM[] memory temp = new VirtualPoolVM[](vPools.length);

//         for (uint256 i = 0; i < vPools.length; i++) {
//             temp[i].fee = vPools[i].fee;
//             temp[i].tokenABalance = vPools[i].tokenABalance;
//             temp[i].tokenBBalance = vPools[i].tokenBBalance;
//             temp[i].tokenAName = vPools[i].tokenAName;
//             temp[i].tokenBName = vPools[i].tokenBName;
//         }
//         return temp;
//     }

//       function getRPools128(Pool128[] storage rPools)
//         public
//         view
//         returns (PoolVM128[] memory)
//     {
//         PoolVM128[] memory temp = new PoolVM128[](rPools.length);

//         for (uint256 i = 0; i < rPools.length; i++) {
//             temp[i].tokenA = rPools[i].tokenA;
//             temp[i].tokenB = rPools[i].tokenB;
//             temp[i].fee = rPools[i].fee;
//             temp[i].reserveRatio = rPools[i].reserveRatio;
//             temp[i].belowReserve = rPools[i].belowReserve;
//             temp[i].tokenABalance = rPools[i].tokenABalance;
//             temp[i].tokenBBalance = rPools[i].tokenBBalance;
//             temp[i].maxReserveRatio = rPools[i].maxReserveRatio;
//         }
//         return temp;
//     }


//     function getVPools128(VirtualPool128[] storage vPools)
//         public
//         view
//         returns (VirtualPoolVM128[] memory)
//     {
//         VirtualPoolVM128[] memory temp = new VirtualPoolVM128[](vPools.length);

//         for (uint256 i = 0; i < vPools.length; i++) {
//             temp[i].fee = vPools[i].fee;
//             temp[i].tokenABalance = vPools[i].tokenABalance;
//             temp[i].tokenBBalance = vPools[i].tokenBBalance;
//             temp[i].tokenAName = vPools[i].tokenAName;
//             temp[i].tokenBName = vPools[i].tokenBName;
//         }
//         return temp;
//     }
// }
