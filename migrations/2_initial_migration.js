const VPool = artifacts.require("vPool");
const ComputationsLibrary = artifacts.require("vPoolCalculations");
// const Migrations = artifacts.require("vPool");

module.exports = function(deployer) {
    deployer.deploy(ComputationsLibrary, { gas: 6721975 });
    deployer.link(ComputationsLibrary,VPool);
    deployer.deploy(VPool, { gas: 6721975 });
};