const ERC20 = artifacts.require("./ERC20/ERC20.sol");
const Migrations = artifacts.require("vPool");

module.exports = function (deployer) {
  const tokens = [
    { name: "Bitcoin", sym: "BTC" }, // 0x82f368Cc874e118F291CF9B05b7545C959143F09
    { name: "Ethereum", sym: "ETH" }, // 0xbB53B6166CaabA8b867A1e3f629D7480af600dA6
    { name: "USDT", sym: "USDT" }, // 0x4903730211dCA46aD3E2381fED1437FC6D7a4BB5
    { name: "USDC", sym: "USDC" }, // 0x3a52dAB965789bB0F894F9895758C9946c842a64
    { name: "Link", sym: "Link" }, // 0x0cb53b315116604F9E7E1821A319E9C411e75044
    { name: "HEX", sym: "HEX" }, // 0x1984becF3b98869F108B19131E1ac8D8AcfAda05
    { name: "Luna", sym: "LUNA" }, // 0xAC6B493CdbaE10e2Fe979c78729Cb21048A85391
    { name: "Wrapper Doge", sym: "WDOGE" }, // 0x1F0F4888355F1Fe6D5fa61Ea8A33CE4DA48C7fa5
    { name: "Maker", sym: "MKR" }, // 0x84ec3E2FFC8c31b2b2CdfB984a7A0BE9629FEb1D
    { name: "Matic", sym: "MATIC" }, //0x4a3C086942E2a5EBb50e1Fd1E21cCA860bf3172C
    { name: "SAND", sym: "SAND" }, //0xF803362588101E59bE5EAef6fb0d4E787F672C06
    { name: "1INCH", sym: "1INCH" }, //0xA0aB83FD911b94141e03902877972829811D581b
    { name: "AAVE", sym: "AAVE" }, //0xfdd57c8BF520bce1813629F6d6C6285C763e6663
  ];

  tokens.forEach((token) => {
    deployer.deploy(ERC20, tokens[0].name, tokens[0].sym);
  });
};
