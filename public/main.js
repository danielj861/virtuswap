function showOverlay() {
    $('.overlay').show();
}

function hideOverlay() {
    $('.overlay').hide();
}

function renderStatePanel() {
    showOverlay();
    $.get("/api/status").then(res => {
        let stateHtml = `Contract: ${res.contractAddress}<br>
                   Account: ${res.accountAddress} (${res.accountBalance} ETH) <br>
                   Pools ${res.poolsInitialized ? "initialized (" + res.poolsCount + " pools)" : "not initialized (0 pools)"}`;


        $('.statePanel').html(stateHtml);

        if (res.poolsInitialized) {
            renderPools();
        }

        hideOverlay();
    });
}

function calculateVpools() {
    showOverlay();
    $.get("/api/calculateVpools").then(res => {
        renderStatePanel();
        hideOverlay();
    });
}

function exchangeReserves() {
    showOverlay();
    $.get("/api/exchangeReserves").then(res => {
        renderStatePanel();
        hideOverlay();
    });
}

function initPools() {
    showOverlay();
    $.get("/api/initPools").then(res => {
        renderStatePanel();
        hideOverlay();
    });
}

function calculateThreshold() {
    showOverlay();
    $.get("/api/calculateBelowThreshold").then(res => {
        renderStatePanel();
        hideOverlay();
    });
}

async function getPoolReserves(tokenA, tokenB) {
    showOverlay();
    $.get(`/api/getPoolReserves?tokenA=${tokenA}&tokenB=${tokenB}`).then(res => {
        let reservesHtml = '';
        for (let i = 0; i < res.length; i++) {
            reservesHtml +=
                `<tr><td>${res[i][1]}</td>
                <td>${res[i][2]}</td>
               </tr>`;
        }

        $('#poolReservesTbl tbody').html(reservesHtml);

        hideOverlay();
    });
}

function showSwap() {
    $('.swapPopup').addClass('show');
    showOverlay();

}

function makeSwap() {
    let selectedinToken = document.getElementById("currencyIn").value;
    let selectedoutToken = document.getElementById("currencyOut").value;

    let amount = document.getElementById("amountIn").value;

    $('.swapPopup').removeClass('show');
    $.get(`/api/swap?tokenIn=${selectedinToken}&tokenOut=${selectedoutToken}&amount=${amount}`).then(res => {
        renderStatePanel();
        hideOverlay();
    });
}

function quote() {
    let selectedinToken = document.getElementById("currencyIn").value;
    let selectedoutToken = document.getElementById("currencyOut").value;

    let amount = document.getElementById("amountIn").value;

    $.get(`/api/quote?tokenIn=${selectedinToken}&tokenOut=${selectedoutToken}&amount=${amount}`).then(res => {
        renderStatePanel();
        alert("quote: " + res);
        hideOverlay();
    });
}

function IndirectCost() {
    $.get(`/api/costUniswapIndirect`).then(res => {
        renderStatePanel();
        alert("quote: " + res);
        hideOverlay();
    });
}

function virtuswapCost() {
    $.get(`/api/virtuswapCost`).then(res => {
        renderStatePanel();
        alert("quote: " + res);
        hideOverlay();
    });
}



function directCost() {
    $.get(`/api/costUniswapdirect`).then(res => {
        renderStatePanel();
        alert("quote: " + res);
        hideOverlay();
    });
}

function testNums() {
    showOverlay();
    $.get("/api/testNums").then(res => {
        renderStatePanel();
        hideOverlay();
    });
}


function calculateReserveRatio() {
    showOverlay();
    $.get("/api/calculateReserve").then(res => {
        renderStatePanel();
        hideOverlay();
    });
}

function renderPools() {
    showOverlay();
    $.get("/api/getRPools").then(res => {

        let tableHtml = '';
        let a = res;
        for (let i = 0; i < res.rPools.length; i++) {
            tableHtml +=
                `<tr class="rpoolToken" data-tokenA="${res.rPools[i].tokenA.tokenAddress}" data-tokenB="${res.rPools[i].tokenB.tokenAddress}"><td>${i}</td><td>
                    ${res.rPools[i].tokenA.address} / ${res.rPools[i].tokenB}
                </td>
                <td>
                    ${res.rPools[i].tokenABalance} / ${res.rPools[i].tokenBBalance}
                </td>
                <td>
                    ${res.rPools[i].belowReserve}
                </td>
                <td>
                    ${res.rPools[i].fee}%
                </td>
                <td>
                    ${res.rPools[i].reserveRatio}%
                </td>
                <td>
                    ${res.rPools[i].maxReserveRatio}%
                </td></tr>`;
        }

        $('#rPools tbody').html(tableHtml);

        // let vPoolsHtml = '';

        // for (let i = 0; i < res.vPools.length; i++) {
        //     vPoolsHtml +=
        //         `<tr><td>${i}</td><td>
        //             ${res.vPools[i].tokenAName} / ${res.vPools[i].tokenBName}
        //         </td>
        //         <td>
        //             ${res.vPools[i].tokenABalance} / ${res.vPools[i].tokenBBalance}
        //         </td>
        //         <td>
        //             ${res.vPools[i].fee}%
        //         </td></tr>`;
        // }

        // $('#vPools tbody').html(vPoolsHtml);

        // let tPoolsHtml = '';

        // for (let i = 0; i < res.tPools.length; i++) {
        //     tPoolsHtml += `<tr><td>${i}</td><td>
        //         ${res.tPools[i].tokenAName} / ${res.tPools[i].tokenBName}
        //     </td>
        //     <td>
        //         ${res.tPools[i].tokenABalance} / ${res.tPools[i].tokenBBalance}
        //     </td>
        //     <td>
        //         ${res.tPools[i].fee}%
        //     </td></tr>`;
        // }

        // $('#tPools tbody').html(tPoolsHtml);

        hideOverlay();
    });

}

function clearLog() {
    $("#textArea").text('');
}

$(function() {
    renderStatePanel();
    $('#initPools').click(initPools);
    $("#refreshPools").click(renderPools);
    $("#calculateReserveRatio").click(calculateReserveRatio);
    $("#calculateThreshold").click(calculateThreshold);
    $("#calculateVpools").click(calculateVpools);
    $("#clearLog").click(clearLog);
    $("#testNums").click(testNums);
    $("#showSwap").click(showSwap);
    $("#swapBtn").click(makeSwap)
    $("#quoteBtn").click(quote)
    $("#exchangeReserves").click(exchangeReserves);
    $("#IndirectCost").click(IndirectCost);
    $("#directCost").click(directCost);
    $("#virtuswapCost").click(virtuswapCost);
    



    $(document).on("click", ".rpoolToken", async(ev) => {
        $('.rpoolToken').removeClass('selected');
        $(ev.currentTarget).addClass('selected');
        await getPoolReserves(ev.currentTarget.dataset.tokena, ev.currentTarget.dataset.tokenb);
    });


    let socket = new WebSocket("ws://localhost:3000/echo");

    socket.onopen = function(e) {};

    socket.onmessage = function(ev) {
        $("#textArea").append(ev.data + "\n");

        var psconsole = $('#textArea');
        if (psconsole.length)
            psconsole.scrollTop(psconsole[0].scrollHeight - psconsole.height());
    };
});